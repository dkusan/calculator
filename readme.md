# README

# Prerequirement

- Za build i run aplikacije potreban je QtCreator (https://www.qt.io/download-open-source).

# Build

- Unutar QtCreator integriranog razvojnog okruženja treba odabrati opciju Build project "Calculator" unutar Build sekcije na alatnoj traci.

``` 
21:38:41: Running steps for project Calculator...
21:38:41: Configuration unchanged, skipping qmake step.
21:38:41: Starting: "/usr/bin/make" -j8
/Applications/Qt/5.12.2/clang_64/bin/qmake -o Makefile ../Calculator/Calculator.pro -spec macx-clang CONFIG+=debug CONFIG+=x86_64 CONFIG+=qml_debug
make: Nothing to be done for `first'.
21:38:41: The process "/usr/bin/make" exited normally.
21:38:41: Elapsed time: 00:00.
```

# Run

- Unutar QtCreator integriranog razvojnog okruženja treba odabrati opciju Run unutar Build sekcije na alatnoj traci.

<img src="calculator.png" width="40%">

# Use

- Program omogučava izvršavanje jednostavnih matematičkih operacija

# Implemented

- Unos pozitivnih i negativnih brojeva
- Unos decimalnog broja
- Jednim odabirom Clear operacije resetira se odabrani broj
- Ponovnim odabirom Clear operacije resetira se stanje kalkulatora
- Zbrajanje i oduzimanje
- Množenje i dijeljenje
- Onemoguceno dijeljenje sa nulom (Reset operacije i ispis upozorenja)
- Uzastopni odabir operacija ponavlja operaciju sa rezultatom predhodne operacije (double calculationNumber)

# Not implemented

- Sve funkcionalnosti jednostavnog kalkulatora su pokrivene i onemoguceno je izazivanje fatalnog errora kroz unos i odabir operacija korisnika
