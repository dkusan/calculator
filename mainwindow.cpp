#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pushButton_num0,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num1,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num2,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num3,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num4,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num5,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num6,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num7,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num8,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_num9,SIGNAL(released()), this, SLOT(pushButtonNumReleased()));
    connect(ui->pushButton_dot,SIGNAL(released()), this, SLOT(pushButtonDotReleased()));
    connect(ui->pushButton_clear,SIGNAL(released()), this, SLOT(pushButtonClearReleased()));
    connect(ui->pushButton_equals,SIGNAL(released()), this, SLOT(pushButtonEqualsReleased()));
    connect(ui->pushButton_plus,SIGNAL(released()), this, SLOT(pushButtonPlusReleased()));
    connect(ui->pushButton_minus,SIGNAL(released()), this, SLOT(pushButtonMinusReleased()));
    connect(ui->pushButton_divide,SIGNAL(released()), this, SLOT(pushButtonDivideReleased()));
    connect(ui->pushButton_multiply,SIGNAL(released()), this, SLOT(pushButtonMultiplyReleased()));
    connect(ui->pushButton_plus_minus,SIGNAL(released()), this, SLOT(pushButtonPlusMinusReleased()));
    connect(ui->pushButton_percent,SIGNAL(released()), this, SLOT(pushButtonPercentReleased()));

    ui->pushButton_plus->setCheckable(true);
    ui->pushButton_minus->setCheckable(true);
    ui->pushButton_divide->setCheckable(true);
    ui->pushButton_multiply->setCheckable(true);
    ui->pushButton_equals->setCheckable(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::pushButtonNumReleased()
{
    QPushButton * button = (QPushButton*)sender();

    QString displayString;

    if(ui->pushButton_plus->isChecked() || ui->pushButton_minus->isChecked() || ui->pushButton_divide->isChecked() || ui->pushButton_multiply->isChecked()|| ui->pushButton_equals->isChecked()
            || ui->display->text() == "0")
    {
        displayString = button->text();
        ui->pushButton_plus->setChecked(false);
        ui->pushButton_minus->setChecked(false);
        ui->pushButton_divide->setChecked(false);
        ui->pushButton_multiply->setChecked(false);
        ui->pushButton_equals->setChecked(false);
    }
    else
    {
        displayString = ui->display->text() + button->text();
    }

    ui->display->setText(displayString);
}

void MainWindow::pushButtonDotReleased()
{
    if(!ui->display->text().contains('.'))
    {
        ui->display->setText(ui->display->text() + ".");
    }
}

void MainWindow::pushButtonPlusMinusReleased()
{
    double displayNumber;

    displayNumber = ui->display->text().toDouble();
    displayNumber*= -1;

    displayResults(displayNumber);
}

void MainWindow::pushButtonPercentReleased()
{
    double displayNumber;

    displayNumber = ui->display->text().toDouble();
    displayNumber*= 0.01;
    displayResults(displayNumber);
}

void MainWindow::pushButtonClearReleased()
{
    if(ui->display->text() == "0")
    {
        action = nothing;
        calculationNumber = 0;
    }
    ui->display->setText("0");
}

void MainWindow::pushButtonEqualsReleased()
{
    calculateResults();

    action = nothing;
}

void MainWindow::pushButtonPlusReleased()
{
    calculateResults();

    action = plus;
}

void MainWindow::pushButtonMinusReleased()
{
    calculateResults();

    action = minus;
}

void MainWindow::pushButtonDivideReleased()
{
    calculateResults();

    action = divide;
}

void MainWindow::pushButtonMultiplyReleased()
{
    calculateResults();

    action = multiply;
}

void MainWindow::calculateResults()
{
    double displayNumber = ui->display->text().toDouble();

    switch(action)
    {
        case(plus):
        {
            displayNumber += calculationNumber;
            break;
        }
        case(minus):
        {
            displayNumber = calculationNumber - displayNumber;
            break;
        }
        case(divide):
        {
            if(!displayNumber || !calculationNumber)
            {
                pushButtonClearReleased();
                ui->display->setText(divideByZeroErr);
                return ;
            }
            displayNumber = calculationNumber / displayNumber;
            break;
        }
        case(multiply):
        {
            displayNumber *= calculationNumber;
            break;
        }
        case(nothing):{
            break;
        }
    }
    calculationNumber = displayNumber;

    displayResults(displayNumber);
}

void MainWindow::displayResults(double displayNumber)
{
    QString displayString;

    displayString = QString::number(displayNumber,'g', 8);

    ui->display->setText(displayString);
}
