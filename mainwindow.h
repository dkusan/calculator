#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum mathAction
{
    plus = 1,
    minus = 2,
    divide = 3,
    multiply = 4,
    nothing = 0
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    mathAction action = nothing;
    double calculationNumber;
    QString divideByZeroErr = "Err: Divide by 0";

private slots:
    void pushButtonNumReleased();
    void pushButtonDotReleased();
    void pushButtonPlusMinusReleased();
    void pushButtonPercentReleased();
    void pushButtonClearReleased();
    void pushButtonEqualsReleased();
    void pushButtonPlusReleased();
    void pushButtonMinusReleased();
    void pushButtonDivideReleased();
    void pushButtonMultiplyReleased();
    void calculateResults();
    void displayResults(double displayNum);
};

#endif // MAINWINDOW_H
